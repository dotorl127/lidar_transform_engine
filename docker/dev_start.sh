xhost +local:docker
docker run --rm -m 8GB -it \
--privileged \
--net=host \
--gpus 'all,"capabilities=compute,utility,graphics"' \
--name lidar_transform_engine \
-u "$(id -u $USER):$(id -g $USER)" \
-e DISPLAY=unix"${DISPLAY}" \
-v /tmp/.X11-unix:/tmp/.X11-unix \
-v "${HOME}"/.Xauthority:/root/.Xauthority:rw \
-v /media/nas_db:/media/data_storage \
-v /media/server_storage:/media/server_storage \
-v /dev/shm/:/dev/shm \
-w "/media/server_storage/repositories/moon/engine_grpc" \
lidar_transform_engine:latest /bin/bash
xhost -local:docker
