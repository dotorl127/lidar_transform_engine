import numpy as np


def range2points(img, min_range=0.0, ver_res=None, up_pitch=15, down_pitch=25):
    # preprocess yaw array shape : (64, 1024)
    yaw = np.linspace(np.pi, -np.pi, 1024)
    yaw = np.tile(yaw, [img.shape[1], 1])

    # preprocess pitch array shape : (64, 1024)
    if ver_res is None:
        up_pitch = 90 - abs(up_pitch)
        down_pitch = 90 + abs(down_pitch)
        pitch = np.linspace(up_pitch, down_pitch, img.shape[1])
        pitch = np.deg2rad(pitch)
        pitch = np.tile(pitch, [1024, 1]).T
    # else:
    #     bin = [-25.0, -14.638, -10.2637, -7.91, -6.4063, -5.407, -4.6492, -4.3864,
    #            -4.0534, -3.6492, -3.2973, -3.0179, -2.667, -2.2794, -1.9821, -1.6312,
    #            -1.333, -1.0179, -0.667, -0.333, -0.0179, 0.2972, 0.7028, 1.0,
    #            1.2972, 1.6849, 2.2794, 3.2973, 4.6136, 7.0176, 10.2983, 15.0167]
    #     ver_res = bin
    #     ver_res = np.deg2rad(sorted([90 - b if b > 0 else b * -1 + 90 for b in ver_res]))
    #     pitch = np.tile(ver_res, [1024, 1]).T
    #
    #     ver_res = np.deg2rad(np.arange(32) + 75)
    #     mask = np.logical_and(yaw >= np.pi / 2, yaw <= -np.pi / 2)
    #     pitch = np.tile(ver_res, [1024, 1]).T

    # calculate range
    if img.shape[0] == 3:  # for 3ch image
        img *= 256
        range = img[0, :] + (img[1, :] * 256) + (img[2, :] * 256 ** 2)
        range = range / (256 ** 3 - 1)
        range = range * 1000
    else:  # for 1ch image
        range = img.squeeze()
        range *= 200

    # masking invalid range value
    mask = np.logical_and(range > min_range, range < 200.0)
    mask = mask.reshape(mask.shape[0] * mask.shape[1])

    x = range * np.sin(pitch) * np.cos(yaw)
    y = range * np.sin(pitch) * np.sin(yaw)
    z = range * np.cos(pitch)
    x = x.reshape(x.shape[0] * x.shape[1])
    y = y.reshape(y.shape[0] * y.shape[1])
    z = z.reshape(z.shape[0] * z.shape[1])

    # concatenate each axis values to completion point cloud data
    points = np.concatenate((x, y, z))
    points = points.reshape(3, -1).T[mask]

    return points
