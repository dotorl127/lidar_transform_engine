import grpc
from concurrent import futures
import file_transfer_pb2
import file_transfer_pb2_grpc

import os
import argparse
from datetime import datetime
import yaml
from tqdm import tqdm
import numpy as np

import torch
from pyquaternion import Quaternion as Q
import open3d as o3d
import cv2

# liif model library
from liif import models
from liif.utils_moon.points2range import spherical_projection
from liif.utils_moon.range2points import range2points
from liif.utils import to_pixel_samples, make_coord_moon

# iln model library
from iln_moon.python_src.models.iln.iln import ILN
from iln_moon.python_src.models.model_utils import generate_model
from utils import generate_laser_directions, normalization_queries, range_image_to_points


class FileTransferServicer(file_transfer_pb2_grpc.FileTransferServicer):
    def __init__(self, server):
        self.server = server

    def parse_config(self,
                     src_dir_path, tgt_dir_path,
                     src_sensor_profile_path, transform_config_path):
        print('config parsing')
        config = {}

        with open(src_sensor_profile_path, 'r') as f:
            src_profile = yaml.load(f, Loader=yaml.FullLoader)

        with open(transform_config_path, 'r') as f:
            tf_config = yaml.load(f, Loader=yaml.FullLoader)

        print(tf_config)

        config['source_dir'] = src_dir_path
        config['target_dir'] = tgt_dir_path

        config['x_diff'] = float(tf_config['delta_position']['x'])
        config['y_diff'] = float(tf_config['delta_position']['y'])
        config['z_diff'] = float(tf_config['delta_position']['z'])

        config['roll_diff'] = np.deg2rad(float(tf_config['delta_angle']['roll']))
        config['pitch_diff'] = np.deg2rad(float(tf_config['delta_angle']['pitch']))
        config['yaw_diff'] = np.deg2rad(float(tf_config['delta_angle']['yaw']))
        rt_mat = np.eye(4)
        rt_mat[:3, :3] = Q(axis=[1, 0, 0], angle=-config['roll_diff']).rotation_matrix @ \
                         Q(axis=[0, 1, 0], angle=-config['pitch_diff']).rotation_matrix @ \
                         Q(axis=[0, 0, 1], angle=-config['yaw_diff']).rotation_matrix
        config['rt_mat'] = rt_mat

        fov = float(src_profile['fov']['vertical']['max']) - float(src_profile['fov']['vertical']['min'])
        if src_profile['lidar_channel']['vertical'] is not None:
            config['src_ver_res'] = int(src_profile['lidar_channel']['vertical'])
        config['src_ver_ang'] = None
        if src_profile['lidar_resolution']['vertical'] is not None:
            if type(src_profile['lidar_resolution']['vertical']) is list:
                config['src_ver_ang'] = list(map(float, src_profile['lidar_resolution']['vertical']))
                config['src_ver_res'] = len(config['src_ver_ang'])
            else:
                config['src_ver_res'] = int(fov / float(src_profile['lidar_resolution']['vertical']))
        if src_profile['lidar_channel']['horizontal'] is not None:
            config['src_hoz_res'] = int(src_profile['lidar_channel']['horizontal'])
        if src_profile['lidar_resolution']['horizontal'] is not None:
            config['src_hoz_res'] = int(360. / float(src_profile['lidar_resolution']['horizontal']))
        config['src_up_fov'] = np.deg2rad(float(src_profile['fov']['vertical']['max']))
        config['src_down_fov'] = np.deg2rad(float(src_profile['fov']['vertical']['min']))

        fov = float(tf_config['fov']['vertical']['max']) - float(tf_config['fov']['vertical']['min'])
        if tf_config['lidar_channel']['vertical'] is not None:
            config['tgt_ver_res'] = int(tf_config['lidar_channel']['vertical'])
        config['tgt_ver_ang'] = None
        if tf_config['lidar_resolution']['vertical'] is not None:
            if type(tf_config['lidar_resolution']['vertical']) is list:
                config['tgt_ver_ang'] = list(map(float, tf_config['lidar_resolution']['vertical']))
                config['tgt_ver_res'] = len(config['tgt_ver_ang'])
            else:
                config['tgt_ver_res'] = int(fov / float(tf_config['lidar_resolution']['vertical']))
        if tf_config['lidar_channel']['horizontal'] is not None:
            config['tgt_hoz_res'] = int(tf_config['lidar_channel']['horizontal'])
        if tf_config['lidar_resolution']['horizontal'] is not None:
            config['tgt_hoz_res'] = int(360. / float(tf_config['lidar_resolution']['horizontal']))
        config['tgt_up_fov'] = np.deg2rad(float(tf_config['fov']['vertical']['max']))
        config['tgt_down_fov'] = np.deg2rad(float(tf_config['fov']['vertical']['min']))

        config['tgt_z_loc'] = float(src_profile['position']['z']) + float(tf_config['delta_position']['z'])

        config['transform'] = tf_config['tansform_flag']['fov'] | tf_config['tansform_flag']['position'] | tf_config['tansform_flag']['angle']
        if config['src_ver_res'] == config['tgt_ver_res'] and config['src_hoz_res'] == config['tgt_hoz_res'] and \
                config['src_up_fov'] == config['tgt_up_fov'] and config['src_down_fov'] == config['tgt_down_fov'] and \
                config['x_diff'] <= 1e-5 and config['y_diff'] <= 1e-5 and config['z_diff'] <= 1e-5:
            config['transform'] = False

        config['up_scale'] = tf_config['tansform_flag']['fov'] | tf_config['tansform_flag']['resolution_channel']
        if config['tgt_ver_res'] < config['src_ver_res'] or \
                (config['tgt_ver_res'] != config['src_ver_res'] and \
                 config['src_up_fov'] < config['tgt_up_fov'] or config['src_down_fov'] > config['tgt_down_fov']):
            config['up_scale'] = False

        return config

    def save_tgt_profile(self, src_sensor_profile_path, tgt_sensor_profile_path, config):
        print('save target profile')
        with open(src_sensor_profile_path) as f:
            tgt_profile = yaml.load(f, Loader=yaml.FullLoader)

        tgt_profile['lidar_channel']['vertical'] = config['tgt_ver_res']
        tgt_profile['lidar_channel']['horizontal'] = config['tgt_hoz_res']
        tgt_profile['lidar_resolution']['vertical'] = float(np.rad2deg(config['tgt_up_fov'] - config['tgt_down_fov'])) / float(config['tgt_ver_res'])
        tgt_profile['lidar_resolution']['horizontal'] = 360.0 / float(config['tgt_hoz_res'])
        tgt_profile['position']['x'] += config['x_diff']
        tgt_profile['position']['y'] += config['y_diff']
        tgt_profile['position']['z'] += config['z_diff']
        tgt_profile['angle']['roll'] += float(np.rad2deg(config['roll_diff']))
        tgt_profile['angle']['pitch'] += float(np.rad2deg(config['pitch_diff']))
        tgt_profile['angle']['yaw'] += float(np.rad2deg(config['yaw_diff']))
        tgt_profile['fov']['vertical']['min'] = float(np.rad2deg(config['tgt_down_fov']))
        tgt_profile['fov']['vertical']['max'] = float(np.rad2deg(config['tgt_up_fov']))

        with open(tgt_sensor_profile_path, 'w') as output_f:
            yaml.dump(tgt_profile, output_f, sort_keys=False)

    def lidar_validation(self,
                         src_dir_path, src_sensor_profile_path,
                         tgt_sensor_profile_path, transform_config_path):
        print('validation test')
        config = self.parse_config(src_dir_path, '', src_sensor_profile_path, transform_config_path)
        print(config)

        if config['x_diff'] >= 1.5 or config['y_diff'] >= 1.5 or config['z_diff'] >= 1.5 or \
                config['roll_diff'] > np.deg2rad(15.0) or config['pitch_diff'] > np.deg2rad(15.0):
            print('over location, roll, picth difference limit')
            return False

        if (config['tgt_up_fov'] - config['src_up_fov']) >= np.deg2rad(15.0) or \
                (config['src_down_fov'] - config['tgt_down_fov']) >= np.deg2rad(15.0):
            print('over fov limit')
            return False
        else:
            ver_ang_res = (config['src_up_fov'] - config['src_down_fov']) / config['src_ver_res']

            if config['src_up_fov'] != config['tgt_up_fov']:
                config['src_up_fov'] = config['tgt_up_fov']
            if config['src_down_fov'] != config['tgt_down_fov']:
                config['src_down_fov'] = config['tgt_down_fov']

            config['src_ver_res'] = int((config['src_up_fov'] - config['src_down_fov']) / ver_ang_res)
            if config['src_ver_res'] < 129:
                if config['src_ver_res'] > config['tgt_ver_res']:
                    config['tgt_ver_res'] = config['src_ver_res']
            else:
                print('over mixmum channels limit')
                return False

        import random
        f_list = sorted(os.listdir(config['source_dir']))
        sample_list = random.sample(f_list, int(len(f_list) * 0.01))

        for frame, f in enumerate(sample_list):
            ext = f.split('.')[-1]
            if ext == 'npy':
                points = np.load(f'{config["source_dir"]}/{f}')[:, :3]
            elif ext == 'pcd':
                pcd = o3d.io.read_point_cloud(f'{config["source_dir"]}/{f}')
                points = np.asarray(pcd.points)[:, :3]
            elif ext == 'bin':
                points = np.fromfile(f'{config["source_dir"]}/{f}', dtype=np.float32)
                points = points.reshape((-1, 4))[:, :3]

            if config['src_up_fov'] > config['tgt_up_fov'] and config['src_down_fov'] < config['tgt_down_fov']:
                channels = np.linspace(config['src_up_fov'], config['src_down_fov'], config['src_ver_res'])
                src_ver_res = channels[channels <= config['tgt_up_fov']]
                src_ver_res = src_ver_res[src_ver_res >= config['tgt_down_fov']]
                config['src_ver_res'] = len(src_ver_res)

            src_img = spherical_projection(config['src_ver_res'], config['src_hoz_res'], points,
                                           up_fov=config['src_up_fov'], down_fov=config['src_down_fov'], ver_res=config['src_ver_ang'])
            src_num_zeros = len(src_img[src_img == 0])

            points[:, 0] -= config['x_diff']
            points[:, 1] -= config['y_diff']
            points[:, 2] -= config['z_diff']
            points = np.hstack((points, np.ones((points.shape[0], 1))))
            points = points @ config['rt_mat']

            inp_img = spherical_projection(config['src_ver_res'], config['src_hoz_res'], points,
                                           up_fov=config['src_up_fov'], down_fov=config['src_down_fov'], ver_res=config['tgt_ver_ang'])

            if float(len(inp_img[inp_img == 0]) / src_num_zeros) >= 1.30:
                print('validation test fail')
                return False

        self.save_tgt_profile(src_sensor_profile_path, tgt_sensor_profile_path, config)

        return True

    def load_model(self, liif_model_path, iln_model_path, config):
        print('load models')
        # liif model
        liif_model = None
        if config['transform']:
            liif_model = models.make(torch.load(liif_model_path)['model'], load_sd=True).cuda().eval()
            liif_model.eval().cuda()

        # iln model
        iln_model = None
        if config['up_scale']:
            check_point = torch.load(iln_model_path)
            iln_model = generate_model(check_point['model']['name'], check_point['model']['args'])
            iln_model.load_state_dict(check_point['model']['state_dict'])
            iln_model.eval().cuda()

        return liif_model, iln_model

    def denoise(self, pred):
        left = np.roll(pred, 1, axis=1)
        right = np.roll(pred, -1, axis=1)

        left_idx = abs(pred - left) >= (0.2 / 200.0)
        right_idx = abs(pred - right) >= (0.2 / 200.0)

        pred[np.logical_and(left_idx, right_idx)] = 0.0

        return pred

    def lidar_transform(self,
                        src_directory_path, src_GT_directory_path,
                        tgt_directory_path, tgt_GT_directory_path,
                        src_sensor_profile_path, tgt_sensor_profile_path,
                        transform_config_path):
        print('transform start')
        coord = None

        config = self.parse_config(src_directory_path, tgt_directory_path,
                                   src_sensor_profile_path, transform_config_path)

        os.makedirs(config['target_dir'], exist_ok=True)

        liif_model, iln_model = self.load_model('model/liif.pth', 'model/iln.pth', config)

        if config['up_scale']:
            query_lasers = generate_laser_directions(config)
            query_lasers = normalization_queries(query_lasers, config)
            query_lasers = torch.from_numpy(query_lasers)[None, :, :].cuda()

        f_list = sorted(os.listdir(config['source_dir']))
        ext = f_list[0][-3:]

        total_latency = 0

        if not os.path.exists(f'{tgt_GT_directory_path}/visualize'):
            os.mkdir(f'{tgt_GT_directory_path}/visualize')

        for frame, f in enumerate(tqdm(f_list)):
            start_t = datetime.now()

            if ext == 'npy':
                points = np.load(f'{config["source_dir"]}/{f}')[:, :3]
            elif ext == 'pcd':
                pcd = o3d.io.read_point_cloud(f'{config["source_dir"]}/{f}')
                points = np.asarray(pcd.points)[:, :3]
            elif ext == 'bin':
                points = np.fromfile(f'{config["source_dir"]}/{f}', dtype=np.float32)
                points = points.reshape((-1, 4))[:, :3]

            before_img = spherical_projection(config['src_ver_res'], config['src_hoz_res'], points,
                                              up_fov=config['src_up_fov'], down_fov=config['src_down_fov'], ver_res=config['src_ver_ang'])

            points[:, 0] -= config['x_diff']
            points[:, 1] -= config['y_diff']
            points[:, 2] -= config['z_diff']
            points = np.hstack((points, np.ones((points.shape[0], 1))))
            points = points @ config['rt_mat']

            inp_img = spherical_projection(config['src_ver_res'], config['src_hoz_res'], points,
                                           up_fov=config['src_up_fov'], down_fov=config['src_down_fov'], ver_res=config['src_ver_ang'])

            inp_img = inp_img.reshape((1, 1, *inp_img.shape))
            inp_img = torch.from_numpy(inp_img / 200).cuda()

            if coord is None:
                coord, _ = to_pixel_samples(inp_img)
                coord = coord.unsqueeze(0).cuda()

            pred = None

            if liif_model is not None:
                with torch.no_grad():
                    pred = liif_model(inp_img, coord)

            if iln_model is not None:
                if pred is not None:
                    pred = pred.view(config['src_ver_res'], config['src_hoz_res'], 1).permute(2, 0, 1).unsqueeze(0)
                else:
                    pred = inp_img

                with torch.no_grad():
                    pred = iln_model(pred, query_lasers)

            sa = (config['tgt_z_loc'] / np.tan(abs(config['tgt_down_fov']))) / 200.0
            pred[pred <= sa] = 0.0
            pred = self.denoise(pred.reshape(config['tgt_ver_res'], config['tgt_hoz_res']).cpu().numpy())

            if config['up_scale']:
                pred = pred.reshape(config['tgt_ver_res'], config['tgt_hoz_res']) * 200.0
                pred_pt = range_image_to_points(range_image=pred, config=config, remove_zero_range=True)
            else:
                pred = pred.view(config['tgt_ver_res'], config['tgt_hoz_res'], 1).permute(2, 0, 1)
                pred_pt = range2points(pred, ver_res=config['tgt_ver_ang'],
                                       up_pitch=config['tgt_up_fov'], down_pitch=config['tgt_down_fov'])

            np.concatenate([pred_pt, np.zeros((pred_pt.shape[0], 1))], axis=1)  # add zero instead intensity
            np.save(f'{config["target_dir"]}/{frame:06d}', pred_pt)

            if os.path.isfile(f'{src_GT_directory_path}/{frame:06d}.txt'):
                with open(f'{src_GT_directory_path}/{frame:06d}.txt', 'r') as src_f:
                    lines = src_f.readlines()

                    with open(f'{tgt_GT_directory_path}/{frame:06d}.txt', 'w') as tgt_f:
                        for line in lines:
                            tgt_f.write(line)
            else:
                with open(f'{tgt_GT_directory_path}/{frame:06d}.txt', 'w') as tgt_f:
                    tgt_f.write('dummy')

            total_latency += (datetime.now() - start_t).microseconds * 1e-3

            before_img = np.expand_dims(before_img, axis=0)
            before_img = before_img.transpose(1, 2, 0)
            before_img = (before_img / 200.0 * 1000).astype(np.uint8)
            before_img = cv2.applyColorMap(before_img, cv2.COLORMAP_VIRIDIS)
            cv2.imwrite(f'{tgt_GT_directory_path}/visualize/{frame:06d}_before.jpg', before_img)

            after_img = pred.reshape(config['tgt_ver_res'], config['tgt_hoz_res'], 1)
            after_img = (after_img / 200.0 * 1000).astype(np.uint8)
            after_img = cv2.applyColorMap(after_img, cv2.COLORMAP_VIRIDIS)
            cv2.imwrite(f'{tgt_GT_directory_path}/visualize/{frame:06d}_after.jpg', after_img)

        print(total_latency / len(f_list))

    def Transfer(self, request, context):
        src_directory_path = request.src_directory_path
        src_GT_directory_path = request.src_GT_directory_path
        tgt_directory_path = request.tgt_directory_path
        tgt_GT_directory_path = request.tgt_GT_directory_path
        src_sensor_profile_path = request.src_sensor_profile_path
        tgt_sensor_profile_path = request.tgt_sensor_profile_path
        transform_config_path = request.transform_config_path

        # 인자 확인용 코드
        print("Received request with the following arguments:")
        print(f"src_directory_path: {src_directory_path}")
        print(f"src_GT_directory_path: {src_GT_directory_path}")
        print(f"tgt_directory_path: {tgt_directory_path}")
        print(f"tgt_GT_directory_path: {tgt_GT_directory_path}")
        print(f"src_sensor_profile_path: {src_sensor_profile_path}")
        print(f"tgt_sensor_profile_path: {tgt_sensor_profile_path}")
        print(f"transform_config_path: {transform_config_path}")

        success = False

        if self.lidar_validation(src_directory_path,
                                 src_sensor_profile_path,
                                 tgt_sensor_profile_path,
                                 transform_config_path):
            self.lidar_transform(src_directory_path, src_GT_directory_path,
                                 tgt_directory_path, tgt_GT_directory_path,
                                 src_sensor_profile_path, tgt_sensor_profile_path,
                                 transform_config_path)
            success = True

        # 서버 종료 (엔진 코드 테스트 후에 삭제해주세요)
        # self.server.stop(0)

        return file_transfer_pb2.TransferResponse(success=success)


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    file_transfer_pb2_grpc.add_FileTransferServicer_to_server(
        FileTransferServicer(server), server)
    server.add_insecure_port("[::]:50051")
    server.start()
    print('server ready')
    server.wait_for_termination()


if __name__ == "__main__":
    serve()
