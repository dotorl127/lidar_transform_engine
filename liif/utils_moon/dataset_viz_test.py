import os
import numpy as np
# from .points2range import generate_range_image
import cv2


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--root_path', type=str, default=None, help='specify location root dataset')
    args = parser.parse_args()

    raw_data_path = args.root_path
    # src_data_path = raw_data_path + '/_out_src'
    # dst_data_path = raw_data_path + '/_out_dst'

    src_data_path = raw_data_path + '/train'
    dst_data_path = raw_data_path + '/GT'

    src_pc_lst = sorted(os.listdir(src_data_path))
    dst_pc_lst = sorted(os.listdir(dst_data_path))
    if os.path.isfile(f'{raw_data_path}/diff.txt'):
        with open(f'{raw_data_path}/diff.txt', 'r') as f:
            diff = f.readlines()
            diff = list(map(float, diff[0].split(',')))
            print(f'X diff : {diff[0]:.2f}')
            print(f'Y diff : {diff[1]:.2f}')
            print(f'Z diff : {diff[2]:.2f}')
    else:
        diff = None

    for s_pc_n, d_pc_n in zip(src_pc_lst, dst_pc_lst):
        # src_img = generate_range_image(file_name=s_pc_n, proj_H=128, src_loc=f'{src_data_path}')
        # s2d_img = generate_range_image(file_name=s_pc_n, proj_H=128, src_loc=f'{src_data_path}', diff=diff)
        # dst_img = generate_range_image(file_name=d_pc_n, proj_H=128, src_loc=f'{dst_data_path}')

        src_img = np.load(f'{src_data_path}/{s_pc_n}')
        dst_img = np.load(f'{dst_data_path}/{d_pc_n}')

        img = np.ones((64*2+1, 1024, 1))
        img[:64, :] = src_img.transpose(1, 2, 0) / 200
        img[64+1:, :] = dst_img.transpose(1, 2, 0) / 200
        img = (img * 1000).astype(np.uint8)
        img = cv2.applyColorMap(img, cv2.COLORMAP_VIRIDIS)

        cv2.imshow('image', img)
        cv2.waitKey(0)
