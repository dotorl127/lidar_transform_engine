import os

import numpy as np

from points2range import spherical_projection
from range2points import range2points
from pyquaternion import Quaternion as Q
import cv2


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset_path', type=str, default=None, help='specify location root dataset')
    parser.add_argument('--diff', type=str, default=None,
                        help='specify difference between source, target LiDAR position and rotation')
    parser.add_argument('--fov', type=str, default='2.0, 24.8')
    parser.add_argument('--ch', type=int, default=64)
    args = parser.parse_args()

    # up_fov, down_fov = map(float, args.fov.split(', '))
    # x_diff, y_diff, z_diff, roll, pitch, yaw = map(float, args.diff.split(','))
    # rt_mat = np.eye(4)
    # rt_mat[:3, :3] = Q(axis=[1, 0, 0], angle=np.deg2rad(-roll)).rotation_matrix @ \
    #                  Q(axis=[0, 1, 0], angle=np.deg2rad(-pitch)).rotation_matrix @ \
    #                  Q(axis=[0, 0, 1], angle=np.deg2rad(-yaw)).rotation_matrix

    dst_pc_lst = sorted(os.listdir(args.dataset_path + '/train'))

    for d_pc_n in dst_pc_lst:
        # points = np.fromfile(f'{args.dataset_path}/{d_pc_n}', dtype=np.float32)
        # points = points.reshape((-1, 4))[:, :3]
        #
        # dst_img = spherical_projection(args.ch, 1024, points,
        #                                up_fov=up_fov, down_fov=down_fov, ver_res=None)
        # dst_img = (dst_img.reshape(1, *dst_img.shape)).transpose(1, 2, 0)
        #
        # print(f'loc diff : {x_diff:.2f}, {y_diff:.2f}, {z_diff:.2f} m')
        # print(f'rot diff : {roll:.2f}, {pitch:.2f}, {yaw:.2f} deg')
        # tf_points = np.copy(points)
        # tf_points[:, 0] -= x_diff
        # tf_points[:, 1] -= y_diff
        # tf_points[:, 2] -= z_diff
        #
        # tf_points = np.hstack((tf_points, np.ones((tf_points.shape[0], 1))))
        # tf_points = tf_points @ rt_mat
        #
        # s2d_img = spherical_projection(args.ch, 1024, tf_points,
        #                                up_fov=up_fov, down_fov=down_fov, ver_res=None)
        # s2d_img = s2d_img.reshape((1, 1, *s2d_img.shape)) / 200.0
        #
        # s2d_points = range2points(s2d_img, up_pitch=up_fov, down_pitch=down_fov)
        # s2d_points[:, 0] += x_diff
        # s2d_points[:, 1] += y_diff
        # s2d_points[:, 2] += z_diff
        # s2d_points = np.hstack((s2d_points, np.ones((s2d_points.shape[0], 1))))
        # s2d_points = s2d_points @ np.linalg.inv(rt_mat)
        # s2d_img = spherical_projection(args.ch, 1024, s2d_points,
        #                                up_fov=up_fov, down_fov=down_fov, ver_res=None)
        # s2d_img = s2d_img.reshape((1, *s2d_img.shape)).transpose(1, 2, 0)

        s2d_img = np.load(f'{args.dataset_path}/train/{d_pc_n}').transpose((1, 2, 0))
        dst_img = np.load(f'{args.dataset_path}/GT/{d_pc_n}').transpose((1, 2, 0))

        img = np.ones((args.ch * 2 + 1, 1024, 1))
        img[:args.ch, :] = s2d_img / 200
        img[args.ch + 1:, :] = dst_img / 200
        img = (img * 255).astype(np.uint8)
        img = cv2.applyColorMap(img, cv2.COLORMAP_VIRIDIS)
        img = cv2.resize(img, (1200, 400))

        cv2.imshow('image', img)
        cv2.waitKey(0)
