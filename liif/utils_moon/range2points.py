import numpy as np


def range2points(img, ver_res=None, up_pitch=None, down_pitch=None):
    # preprocess yaw array shape : (64, 1024)
    yaw = np.linspace(np.pi, -np.pi, 1024)
    yaw = np.tile(yaw, [img.shape[1], 1])

    # preprocess pitch array shape : (64, 1024)
    if ver_res is None:
        up_pitch = (np.pi / 2) - abs(up_pitch)
        down_pitch = (np.pi / 2) + abs(down_pitch)
        pitch = np.linspace(up_pitch, down_pitch, img.shape[1])
        pitch = pitch
        pitch = np.tile(pitch, [1024, 1]).T
    else:
        pitch = np.tile(ver_res, [1024, 1]).T

    # calculate range
    if img.shape[0] == 3:  # for 3ch image
        img *= 256
        range = img[0, :] + (img[1, :] * 256) + (img[2, :] * 256 ** 2)
        range = range / (256 ** 3 - 1)
        range = range * 1000
    else:  # for 1ch image
        range = img.squeeze()
        range *= 200

    x = range * np.sin(pitch) * np.cos(yaw)
    y = range * np.sin(pitch) * np.sin(yaw)
    z = range * np.cos(pitch)
    x = x.reshape(x.shape[0] * x.shape[1])
    y = y.reshape(y.shape[0] * y.shape[1])
    z = z.reshape(z.shape[0] * z.shape[1])

    # concatenate each axis values to completion point cloud data
    points = np.concatenate((x, y, z))
    points = points.reshape(3, -1).T

    return points
