import os
import argparse
import curses

import numpy as np
import open3d as o3d

import mayavi.mlab as mlab


def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


try:
    curs = curses.initscr()

    @mlab.animate(delay=500)
    def anim():
        for idx, f in enumerate(f_list):
            curs.addstr(0, 0, f'{idx}/{len(f_list)}')
            curs.refresh()
            pcd = o3d.io.read_point_cloud(f'{args.p}/{f}')
            pts = np.asarray(pcd.points)
            # pts = np.load(f'{args.p}/{f}')
            vis_pts.mlab_source.reset(x=pts[:, 0], y=pts[:, 1], z=pts[:, 2])
            yield


    parser = argparse.ArgumentParser()
    parser.add_argument('-p', type=str, default="/media/moon/extraDB/KETIDBextractor_result/outser_test/LiDAR/LiDAR0")
    parser.add_argument('-s', type=str2bool, default=False)
    parser.add_argument('-v', type=str2bool, default=False)
    args = parser.parse_args()

    f_list = sorted(os.listdir(args.p))

    if args.s:
        print('start convert pcd to npy, save')
        save_path = f'{os.path.join(args.p, os.pardir)}/npy'
        # os.mkdir(save_path)
        for idx, f in enumerate(f_list):
            curs.addstr(0, 0, f'{idx}/{len(f_list)} saved')
            curs.refresh()
            pcd = o3d.io.read_point_cloud(f'{args.p}/{f}')
            filename = f.split('.')[0]
            np.save(f'{save_path}/{filename}', np.asarray(pcd.points))
    elif args.v:
        print('start convert pcd to npy, view')
        pts_fig = mlab.figure(figure='predicted', size=(700, 700), bgcolor=(0.05, 0.05, 0.05))
        vis_pts = mlab.points3d(0, 0, 0, mode='point', figure=pts_fig)
        mlab.view(distance=200.0)
        anim()
        mlab.show()

    print('done')

finally:
    curses.endwin()
