import os
import numpy as np
import multiprocessing
from functools import partial
from tqdm import tqdm
from .range2points import range2points


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def spherical_projection(proj_H, proj_W, points, up_fov, down_fov, ver_res=None):
    range = np.linalg.norm(points, 2, axis=1)
    x_, y_, z_ = points[:, 0], points[:, 1], points[:, 2]

    yaw = np.arctan2(y_, x_)
    pitch = np.arctan2(np.linalg.norm(points[:, :2], 2, axis=1), z_)
    fov = np.abs(up_fov) + np.abs(down_fov)
    up_pitch = (np.pi / 2) - np.abs(up_fov)

    proj_x = 0.5 * (1.0 - yaw / np.pi)
    proj_x *= proj_W
    # image horizontal fov filtering
    proj_x = np.floor(proj_x)
    proj_x = np.minimum(proj_W - 1, proj_x)
    proj_x = np.maximum(0, proj_x).astype(np.int32)

    if ver_res is None:
        proj_y = (pitch - up_pitch) / fov
        proj_y *= proj_H
        # image vertical fov filtering
        proj_y = np.floor(proj_y)
        proj_y = np.minimum(proj_H - 1, proj_y)
        proj_y = np.maximum(0, proj_y).astype(np.int32)
    else:
        proj_y = np.zeros(pitch.shape).astype(np.int32)
        ver_res = np.deg2rad(ver_res)
        ver_res = sorted([(np.pi / 2) - b if b > 0 else b * -1 + (np.pi / 2) for b in ver_res])
        ver_res = np.append(ver_res, np.inf)
        idx = np.digitize(pitch, ver_res)
        idx = np.minimum(proj_H - 1, idx)
        idx = np.maximum(0, idx).astype(np.int32)

    # order in decreasing for make ascending range
    order = np.argsort(range)[::-1]
    range = range[order]
    proj_x = proj_x[order]
    proj_y = proj_y[order]

    spherical_array = np.zeros((proj_H, proj_W), dtype=np.float32)
    spherical_array[proj_y, proj_x] = range

    return spherical_array


def convert_RGB(spherical_array, proj_W, proj_H):
    # convert range to RGB normalized value
    rgb_sum_val = spherical_array / 1000 * (pow(256, 3) - 1)
    b = np.floor(rgb_sum_val / pow(256, 2))
    rg_sum_val = rgb_sum_val - (b * pow(256, 2))
    g = np.floor(rg_sum_val / 256)
    r = np.floor(rg_sum_val - (g * 256))

    range_array = np.zeros((proj_H, proj_W, 3), dtype=np.float32)
    range_array[:, :, 0] = r
    range_array[:, :, 1] = g
    range_array[:, :, 2] = b

    return range_array


def generate_range_image(file_name, src_loc, dst_loc=None,
                         up_fov=15.0, down_fov=25.0, diff=None, proj_H=64, proj_W=1024, reverse=False):
    points = None
    if file_name.split('.')[-1] == 'npy':
        points = np.load(f'{src_loc}/{file_name}')[:, :3]  # for npy file
        img_file_name = str(file_name).rstrip('.npy')
    elif file_name.split('.')[-1] == 'bin':
        points = np.fromfile(f'{src_loc}/{file_name}', dtype=np.float32)
        points = points.reshape((-1, 4))[:, :3]
        img_file_name = str(file_name).rstrip('.bin')

    x = y = z = 0.0
    if diff is not None:
        x, y, z = diff[:3]

    points[:, 0] -= x
    points[:, 1] -= y
    points[:, 2] -= z

    spherical_array = spherical_projection(proj_H, proj_W, points, up_fov=up_fov, down_fov=down_fov)

    # just save 1 channel range npy data
    range_array = spherical_array.reshape((1, *spherical_array.shape))

    if dst_loc is not None:
        if reverse:
            img_file_name += '_'
        np.save(f'{dst_loc}/{img_file_name}.npy', range_array)
    else:
        return range_array


def generate_range_image2(file_name, src_loc, dst_loc=None,
                          up_fov=15.0, down_fov=25.0, diff=None, proj_H=64, proj_W=1024, reverse=False):
    points = None
    if file_name.split('.')[-1] == 'npy':
        points = np.load(f'{src_loc}/{file_name}')[:, :3]  # for npy file
        img_file_name = str(file_name).rstrip('.npy')
    elif file_name.split('.')[-1] == 'bin':
        points = np.fromfile(f'{src_loc}/{file_name}', dtype=np.float32)
        points = points.reshape((-1, 4))[:, :3]
        img_file_name = str(file_name).rstrip('.bin')

    x = y = z = 0.0
    if diff == 'random':
        loc = np.round(np.linspace(-0.5, 0.5, 10), 2)
        x = np.random.choice(loc)
        y = np.random.choice(loc)
        z = np.random.choice(loc)
    elif diff is not None:
        x, y, z = diff[:3]

    points[:, 0] -= x
    points[:, 1] -= y
    points[:, 2] -= z

    spherical_array = spherical_projection(proj_H, proj_W, points, up_fov=up_fov, down_fov=down_fov)

    # just save 1 channel range npy data
    range_array = spherical_array.reshape((1, 1, *spherical_array.shape)) / 200

    points = range2points(range_array, up_pitch=up_fov, down_pitch=down_fov)
    points[:, 0] += x
    points[:, 1] += y
    points[:, 2] += z
    range_array = spherical_projection(proj_H, proj_W, points, up_fov=up_fov, down_fov=down_fov)
    range_array = range_array.reshape((1, *range_array.shape))

    if dst_loc is not None:
        if reverse:
            img_file_name += '_'
        np.save(f'{dst_loc}/{img_file_name}.npy', range_array)
    else:
        return range_array


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--root_path', type=str, default='/media/moon/extraDB/CARLA_TRAIN_DATASET',
                        help='specify location root dataset')
    parser.add_argument('--fov', type=str, default='15, 25', help='specify up fov, degree')
    parser.add_argument('--w', type=int, default=1024, help='specify image size of width, pixel')
    parser.add_argument('--h', type=int, default=64, help='specify image size of height, pixel')
    parser.add_argument('--f', type=str2bool, default=False, help='If False save the exists old dataset.')
    args = parser.parse_args()

    raw_dir_list = sorted(os.listdir(args.root_path))
    up_fov, down_fov = map(float, args.fov.split(','))

    for raw in raw_dir_list:
        raw_data_path = f'{args.root_path}/{raw}'

        if os.path.exists(f'{raw_data_path}/train') and os.path.exists(f'{raw_data_path}/GT'):
            if not args.f:
                print('#' * 10, raw_data_path, '#' * 10)
                print('convert range image has already completed, skip')
                continue

        src_data_path = raw_data_path + '/velodyne_points/data'
        dst_data_path = raw_data_path + '/_out_dst'

        src_pc_lst = os.listdir(src_data_path)
        # dst_pc_lst = os.listdir(dst_data_path)
        if os.path.isfile(f'{raw_data_path}/diff.txt'):
            with open(f'{raw_data_path}/diff.txt', 'r') as f:
                diff = f.readlines()
                diff = list(map(float, diff[0].split(',')))
        else:
            diff = None

        # generate S' from S data
        print('#' * 10, raw_data_path, '#' * 10)
        print('start generate train range image from PCD data.')

        os.makedirs(f'{raw_data_path}/train', exist_ok=True)
        # transform with diff, offset + spherical projection
        convert_fn = partial(generate_range_image2, src_loc=src_data_path, dst_loc=f'{raw_data_path}/train',
                             up_fov=up_fov, down_fov=down_fov, diff='random', proj_W=args.w, proj_H=args.h)
        with multiprocessing.Pool() as p:
            list(tqdm(p.imap(convert_fn, src_pc_lst), total=len(src_pc_lst)))

        # transform with diff, offset + spherical projection
        # diff[0] *= -1
        # diff[1] *= -1
        # diff[2] *= -1
        # convert_fn = partial(generate_range_image, src_loc=dst_data_path, dst_loc=f'{raw_data_path}/train', diff=diff,
        #                      proj_W=args.w, proj_H=args.h, reverse=True)
        # with multiprocessing.Pool() as p:
        #     list(tqdm(p.imap(convert_fn, src_pc_lst), total=len(src_pc_lst)))

        # generate T' from T data
        print('start generate ground-truth range image from PCD data.')

        os.makedirs(f'{raw_data_path}/GT', exist_ok=True)
        # only spherical projection
        convert_fn = partial(generate_range_image, src_loc=src_data_path, dst_loc=f'{raw_data_path}/GT',
                             up_fov=up_fov, down_fov=down_fov, proj_W=args.w, proj_H=args.h)
        with multiprocessing.Pool() as p:
            list(tqdm(p.imap(convert_fn, src_pc_lst), total=len(src_pc_lst)))

        # # only spherical projection
        # convert_fn = partial(generate_range_image, src_loc=src_data_path, dst_loc=f'{raw_data_path}/GT',
        #                      proj_W=args.w, proj_H=args.h, reverse=True)
        # with multiprocessing.Pool() as p:
        #     list(tqdm(p.imap(convert_fn, dst_pc_lst), total=len(dst_pc_lst)))

        print('done.')
