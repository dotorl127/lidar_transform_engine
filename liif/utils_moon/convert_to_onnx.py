import argparse
import sys, os
import numpy as np
import torch
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
import models


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model')
    parser.add_argument('--gpu', default='0')
    parser.add_argument('--output', default='model.onnx')

    args = parser.parse_args()

    os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu

    model = models.make(torch.load(args.model)['model'], load_sd=True).cuda().eval()
    dummy_input = torch.from_numpy(np.ones((1, 1, 64, 1024), dtype=np.float32)).cuda()
    dummy_input2 = torch.from_numpy(np.ones((1, 64 * 1024, 2), dtype=np.float32)).cuda()
    torch.onnx.export(model, (dummy_input, dummy_input2),
                      args.output,
                      input_names=['inp', 'coord'],
                      output_names=['output'],
                      export_params=True,
                      opset_version=16)
