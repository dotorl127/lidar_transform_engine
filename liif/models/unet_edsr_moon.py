from argparse import Namespace
import torch
import torch.nn as nn
from models import register


class DoubleConv(nn.Module):
    def __init__(self, in_channels, out_channels, mid_channels=None):
        super(DoubleConv, self).__init__()
        if not mid_channels:
            mid_channels = out_channels
        self.double_conv = nn.Sequential(nn.Conv2d(in_channels, mid_channels, kernel_size=3, padding=1, bias=True),
                                         nn.BatchNorm2d(mid_channels),
                                         nn.ReLU(inplace=True),  # down block uses leaky ReLu in pix2pix
                                         nn.Conv2d(mid_channels, out_channels, kernel_size=3, padding=1, bias=True),
                                         nn.BatchNorm2d(out_channels),
                                         nn.ReLU(inplace=True))

    def forward(self, x):
        return self.double_conv(x)


class Down(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(Down, self).__init__()
        self.maxpool_conv = nn.Sequential(nn.MaxPool2d(2),
                                          nn.Dropout(0.25),  # no use in pix2pix
                                          DoubleConv(in_channels, out_channels))

    def forward(self, x):
        return self.maxpool_conv(x)


class Down_res(nn.Module):
    def __init__(self, in_channels, out_channels, n_resblocks, res_scale=1):
        super(Down_res, self).__init__()
        act = nn.ReLU(True)

        self.maxpool_conv = nn.Sequential(nn.MaxPool2d(2),
                                          nn.Conv2d(in_channels, out_channels, 1))
        resblocks = [ResBlock(out_channels, 3, act=act, res_scale=res_scale) for _ in range(n_resblocks)]
        self.resblocks = nn.Sequential(*resblocks)

    def forward(self, x):
        x = self.maxpool_conv(x)
        res = self.resblocks(x)
        res += x
        return res


class Up(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(Up, self).__init__()
        if in_channels == out_channels:
            self.up = nn.ConvTranspose2d(in_channels, in_channels, kernel_size=2, stride=2)
            self.conv = nn.Sequential(DoubleConv(in_channels * 2, out_channels),
                                      nn.Dropout(0.25))  # dropout rate 0.5 in pix2pix
        else:
            self.up = nn.ConvTranspose2d(in_channels, in_channels // 2, kernel_size=2, stride=2)
            self.conv = nn.Sequential(DoubleConv(in_channels, out_channels),
                                      nn.Dropout(0.25))

    def forward(self, x1, x2):
        x1 = self.up(x1)
        x = torch.cat([x2, x1], dim=1)
        return self.conv(x)


class OutConv(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(OutConv, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size=1)

    def forward(self, x):
        return self.conv(x)


class UNet(nn.Module):
    def __init__(self):
        super(UNet, self).__init__()
        self.inc = DoubleConv(1, 64)
        self.down1 = Down(64, 128)
        self.down2 = Down(128, 256)
        self.down3 = Down(256, 512)
        self.down4 = Down(512, 512)
        self.down5 = Down(512, 512)
        self.up1 = Up(512, 512)
        self.up2 = Up(512, 512)
        self.up3 = Up(512, 256)
        self.up4 = Up(256, 128)
        self.up5 = Up(128, 64)
        self.outc = OutConv(64, 1)

    def forward(self, x):
        x1 = self.inc(x)
        x2 = self.down1(x1)
        x3 = self.down2(x2)
        x4 = self.down3(x3)
        x5 = self.down4(x4)
        x6 = self.down5(x5)
        x = self.up1(x6, x5)
        x = self.up2(x, x4)
        x = self.up3(x, x3)
        x = self.up4(x, x2)
        x = self.up5(x, x1)
        logits = self.outc(x)
        return logits


class ResBlock(nn.Module):
    def __init__(self, n_feats, kernel_size=3, bias=True, bn=False, act=nn.ReLU(True), res_scale=1):
        super(ResBlock, self).__init__()
        m = []
        for i in range(2):
            m.append(nn.Conv2d(n_feats, n_feats, kernel_size=kernel_size, padding=(kernel_size // 2), bias=bias))
            if bn:
                m.append(nn.BatchNorm2d(n_feats))
            if i == 0:
                m.append(act)

        self.body = nn.Sequential(*m)
        self.res_scale = res_scale

    def forward(self, x):
        res = self.body(x).mul(self.res_scale)
        res += x
        return res


class EDSR(nn.Module):
    def __init__(self, args):
        super(EDSR, self).__init__()
        self.args = args
        n_resblocks = args.n_resblocks
        n_feats = args.n_feats
        kernel_size = 3
        self.channel = 32
        act = nn.ReLU(True)

        m_head = [nn.Conv2d(1, n_feats, kernel_size, padding=(kernel_size // 2))]
        m_body = [ResBlock(n_feats, kernel_size, act=act, res_scale=args.res_scale)] * n_resblocks
        m_body.append(nn.Conv2d(n_feats, n_feats, kernel_size, padding=(kernel_size // 2)))

        m_tail = [nn.Conv2d(n_feats, 1, kernel_size, padding=(kernel_size // 2))]

        self.head = nn.Sequential(*m_head)
        self.body = nn.Sequential(*m_body)
        self.tail = nn.Sequential(*m_tail)

    def forward(self, x):
        x = self.head(x)
        res = self.body(x)
        res += x
        x = self.tail(res)
        return x

    def set_channel(self, channel):
        self.channel = channel


class EDSR_UNET_series(nn.Module):
    def __init__(self, args):
        super(EDSR_UNET_series, self).__init__()
        self.args = args
        self.channel = 32
        self.unet = UNet()
        self.edsr = EDSR(args)

    def forward(self, x):
        x = self.edsr(x)
        x = self.unet(x)
        return x

    def load_state_dict(self, state_dict, strict=True):
        own_state = self.state_dict()
        for name, param in state_dict.items():
            if name in own_state:
                if isinstance(param, nn.Parameter):
                    param = param.data
                try:
                    own_state[name].copy_(param)
                except Exception:
                    if name.find('tail') == -1:
                        raise RuntimeError('While copying the parameter named {}, '
                                           'whose dimensions in the model are {} and '
                                           'whose dimensions in the checkpoint are {}.'
                                           .format(name, own_state[name].size(), param.size()))
            elif strict:
                if name.find('tail') == -1:
                    raise KeyError('unexpected key "{}" in state_dict'
                                   .format(name))

    def set_channel(self, channel):
        self.channel = channel


class UNET_EDSR_series(nn.Module):
    def __init__(self, args):
        super(UNET_EDSR_series, self).__init__()
        self.args = args
        self.channel = 32
        self.unet = UNet()
        self.edsr = EDSR(args)

    def forward(self, x):
        x = self.unet(x)
        x = self.edsr(x)
        return x

    def load_state_dict(self, state_dict, strict=True):
        own_state = self.state_dict()
        for name, param in state_dict.items():
            if name in own_state:
                if isinstance(param, nn.Parameter):
                    param = param.data
                try:
                    own_state[name].copy_(param)
                except Exception:
                    if name.find('tail') == -1:
                        raise RuntimeError('While copying the parameter named {}, '
                                           'whose dimensions in the model are {} and '
                                           'whose dimensions in the checkpoint are {}.'
                                           .format(name, own_state[name].size(), param.size()))
            elif strict:
                if name.find('tail') == -1:
                    raise KeyError('unexpected key "{}" in state_dict'
                                   .format(name))

    def set_channel(self, channel):
        self.channel = channel


class UNET_EDSR_parallel(nn.Module):
    def __init__(self, args):
        super(UNET_EDSR_parallel, self).__init__()
        self.args = args
        self.channel = 32
        self.unet = UNet()
        self.edsr = EDSR(args)

    def forward(self, x):
        unet = self.unet(x)
        x = self.edsr(x).mul(self.args.mul)
        x += unet
        return x

    def load_state_dict(self, state_dict, strict=True):
        own_state = self.state_dict()
        for name, param in state_dict.items():
            if name in own_state:
                if isinstance(param, nn.Parameter):
                    param = param.data
                try:
                    own_state[name].copy_(param)
                except Exception:
                    if name.find('tail') == -1:
                        raise RuntimeError('While copying the parameter named {}, '
                                           'whose dimensions in the model are {} and '
                                           'whose dimensions in the checkpoint are {}.'
                                           .format(name, own_state[name].size(), param.size()))
            elif strict:
                if name.find('tail') == -1:
                    raise KeyError('unexpected key "{}" in state_dict'
                                   .format(name))

    def set_channel(self, channel):
        self.channel = channel


class UNET_res(nn.Module):
    def __init__(self, args):
        super(UNET_res, self).__init__()
        self.args = args
        self.channel = 32
        n_feats = args.n_feats
        n_resblocks = args.n_resblocks
        res_scale = args.res_scale

        self.inc = nn.Conv2d(1, n_feats, 3, padding=1)
        self.down1 = Down_res(64, 128, n_resblocks, res_scale)
        self.down2 = Down_res(128, 256, n_resblocks, res_scale)
        self.down3 = Down_res(256, 512, n_resblocks, res_scale)
        self.down4 = Down_res(512, 512, n_resblocks, res_scale)
        self.down5 = Down_res(512, 512, n_resblocks, res_scale)
        self.up1 = Up(512, 512)
        self.up2 = Up(512, 512)
        self.up3 = Up(512, 256)
        self.up4 = Up(256, 128)
        self.up5 = Up(128, 64)
        self.outc = nn.Conv2d(64, 1, 1)

    def forward(self, x):
        x1 = self.inc(x)
        x2 = self.down1(x1)
        x3 = self.down2(x2)
        x4 = self.down3(x3)
        x5 = self.down4(x4)
        x6 = self.down5(x5)
        x = self.up1(x6, x5)
        x = self.up2(x, x4)
        x = self.up3(x, x3)
        x = self.up4(x, x2)
        x = self.up5(x, x1)
        logits = self.outc(x)
        return logits

    def load_state_dict(self, state_dict, strict=True):
        own_state = self.state_dict()
        for name, param in state_dict.items():
            if name in own_state:
                if isinstance(param, nn.Parameter):
                    param = param.data
                try:
                    own_state[name].copy_(param)
                except Exception:
                    if name.find('tail') == -1:
                        raise RuntimeError('While copying the parameter named {}, '
                                           'whose dimensions in the model are {} and '
                                           'whose dimensions in the checkpoint are {}.'
                                           .format(name, own_state[name].size(), param.size()))
            elif strict:
                if name.find('tail') == -1:
                    raise KeyError('unexpected key "{}" in state_dict'
                                   .format(name))

    def set_channel(self, channel):
        self.channel = channel


@register('unet')
def make_unet_moon():
    return UNet()


@register('edsr-unet-moon-series')
def make_res_unet_moon(n_resblocks=16, n_feats=64, res_scale=1):
    args = Namespace()
    args.n_resblocks = n_resblocks
    args.n_feats = n_feats
    args.res_scale = res_scale
    return EDSR_UNET_series(args)


@register('unet-edsr-moon-series')
def make_res_unet_moon(n_resblocks=16, n_feats=64, res_scale=1):
    args = Namespace()
    args.n_resblocks = n_resblocks
    args.n_feats = n_feats
    args.res_scale = res_scale
    return UNET_EDSR_series(args)


@register('unet-edsr-moon-parallel')
def make_res_unet_moon(n_resblocks=16, n_feats=64, res_scale=1, mul=1):
    args = Namespace()
    args.n_resblocks = n_resblocks
    args.n_feats = n_feats
    args.res_scale = res_scale
    args.mul = mul
    return UNET_EDSR_parallel(args)

