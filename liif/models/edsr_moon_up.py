# modified from: https://github.com/thstkdgus35/EDSR-PyTorch

from argparse import Namespace
import torch.nn as nn
from models import register


class Upsampler(nn.Sequential):
    def __init__(self, n_feats):
        m = [nn.Conv2d(n_feats, n_feats * 4, 3, padding=1, bias=True),
             nn.PixelShuffle(2),
             nn.Conv2d(n_feats, n_feats, 3, padding=1, stride=(1, 2), bias=True)]
        super(Upsampler, self).__init__(*m)


class ResBlock(nn.Module):
    def __init__(self, n_feats, kernel_size, bias=True, bn=False, act=nn.ReLU(True), res_scale=1):

        super(ResBlock, self).__init__()
        m = []
        for i in range(2):
            m.append(nn.Conv2d(n_feats, n_feats, kernel_size, padding=(kernel_size // 2), bias=bias))
            if bn:
                m.append(nn.BatchNorm2d(n_feats))
            if i == 0:
                m.append(act)

        self.body = nn.Sequential(*m)
        self.res_scale = res_scale

    def forward(self, x):
        res = self.body(x).mul(self.res_scale)
        res += x

        return res


class EDSR(nn.Module):
    def __init__(self, args):
        super(EDSR, self).__init__()
        self.args = args
        self.channel = 32
        self.scale_idx = None
        n_resblocks = args.n_resblocks
        n_feats = args.n_feats
        kernel_size = 3
        act = nn.ReLU(True)

        m_head = [nn.Conv2d(args.n_colors, n_feats, kernel_size, padding=(kernel_size // 2))]

        self.preproc = nn.ModuleList([
            nn.Sequential(ResBlock(n_feats, 5, act=act, res_scale=args.res_scale),
                          ResBlock(n_feats, 5, act=act, res_scale=args.res_scale)
                          ) for _ in range(3)
        ])

        m_body = [ResBlock(n_feats, kernel_size, act=act, res_scale=args.res_scale) for _ in range(n_resblocks)]
        m_body.append(nn.Conv2d(n_feats, n_feats, kernel_size, padding=(kernel_size // 2)))

        self.up = nn.ModuleList([Upsampler(64),
                                 Upsampler(64)])

        m_tail = [nn.Conv2d(n_feats, args.n_colors, kernel_size, padding=(kernel_size // 2))]

        self.head = nn.Sequential(*m_head)
        self.body = nn.Sequential(*m_body)
        self.tail = nn.Sequential(*m_tail)

    def forward(self, x):
        x = self.head(x)
        x = self.preproc[self.channel // 32 - 1](x)
        res = self.body(x)
        res += x

        if self.scale_idx is not None:
            if self.scale_idx != 2:
                res = self.up[self.scale_idx](res)
            else:
                for up in self.up:
                    res = up(res)

        x = self.tail(res)
        return x

    def load_state_dict(self, state_dict, strict=True):
        own_state = self.state_dict()
        for name, param in state_dict.items():
            if name in own_state:
                if isinstance(param, nn.Parameter):
                    param = param.data
                try:
                    own_state[name].copy_(param)
                except Exception:
                    if name.find('tail') == -1:
                        raise RuntimeError('While copying the parameter named {}, '
                                           'whose dimensions in the model are {} and '
                                           'whose dimensions in the checkpoint are {}.'
                                           .format(name, own_state[name].size(), param.size()))
            elif strict:
                if name.find('tail') == -1:
                    raise KeyError('unexpected key "{}" in state_dict'
                                   .format(name))

    def set_channel(self, channel):
        self.channel = channel

    def set_scale_idx(self, scale_idx):
        self.scale_idx = scale_idx


@register('edsr-moon-up')
def make_edsr_moon(n_resblocks=16, n_feats=64, res_scale=1,
                   rgb_range=1, n_colors=1):
    args = Namespace()
    args.n_resblocks = n_resblocks
    args.n_feats = n_feats
    args.res_scale = res_scale
    args.rgb_range = rgb_range
    args.n_colors = n_colors
    return EDSR(args)
