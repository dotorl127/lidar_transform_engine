import torch
import torch.nn as nn
import torch.nn.functional as F
from .models import register


class DoubleConv(nn.Module):
    def __init__(self, in_channels, out_channels, mid_channels=None):
        super(DoubleConv, self).__init__()
        if not mid_channels:
            mid_channels = out_channels
        self.double_conv = nn.Sequential(nn.Conv2d(in_channels, mid_channels, kernel_size=3, padding=1, bias=True),
                                         nn.BatchNorm2d(mid_channels),
                                         nn.ReLU(inplace=True),
                                         nn.Conv2d(mid_channels, out_channels, kernel_size=3, padding=1, bias=True),
                                         nn.BatchNorm2d(out_channels),
                                         nn.ReLU(inplace=True))

    def forward(self, x):
        return self.double_conv(x)


class Down(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(Down, self).__init__()
        self.maxpool_conv = nn.Sequential(nn.MaxPool2d(2),
                                          nn.Dropout(0.25),  # no use in pix2pix
                                          DoubleConv(in_channels, out_channels))

    def forward(self, x):
        return self.maxpool_conv(x)


class Up(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(Up, self).__init__()
        if in_channels == out_channels:
            self.up = nn.ConvTranspose2d(in_channels, in_channels, kernel_size=2, stride=2)
            self.conv = nn.Sequential(DoubleConv(in_channels * 2, out_channels),
                                      nn.Dropout(0.25))  # dropout rate 0.5 in pix2pix
        else:
            self.up = nn.ConvTranspose2d(in_channels, in_channels // 2, kernel_size=2, stride=2)
            self.conv = nn.Sequential(DoubleConv(in_channels, out_channels),
                                      nn.Dropout(0.25))

    def forward(self, x1, x2):
        x1 = self.up(x1)
        x = torch.cat([x2, x1], dim=1)
        return self.conv(x)


class OutConv(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(OutConv, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size=1)

    def forward(self, x):
        return self.conv(x)


@register('unet-moon')
class UNet(nn.Module):
    def __init__(self, encoder_spec):
        super(UNet, self).__init__()
        self.inc = DoubleConv(1, 64)
        self.down1 = Down(64, 128)
        self.down2 = Down(128, 256)
        self.down3 = Down(256, 512)
        self.down4 = Down(512, 1024)
        self.down5 = Down(1024, 1024)
        self.down6 = Down(1024, 1024)
        self.up1 = Up(1024, 1024)
        self.up2 = Up(1024, 1024)
        self.up3 = Up(1024, 512)
        self.up4 = Up(512, 256)
        self.up5 = Up(256, 128)
        self.up6 = Up(128, 64)
        self.outc = OutConv(64, 1)

    def forward(self, inp, coord, coord_origin):
        x = F.grid_sample(
            inp, coord.flip(-1).unsqueeze(1),
            mode='nearest', align_corners=False)[:, :, 0, :].permute(0, 2, 1)
        x = x.view(-1, 1, 128, 1024)
        x1 = self.inc(x)
        x2 = self.down1(x1)
        x3 = self.down2(x2)
        x4 = self.down3(x3)
        x5 = self.down4(x4)
        x6 = self.down5(x5)
        x7 = self.down6(x6)
        x = self.up1(x7, x6)
        x = self.up2(x, x5)
        x = self.up3(x, x4)
        x = self.up4(x, x3)
        x = self.up5(x, x2)
        x = self.up6(x, x1)
        x = self.outc(x)
        x = F.grid_sample(
            x, coord_origin.flip(-1).unsqueeze(1),
            mode='nearest', align_corners=False)[:, :, 0, :].permute(0, 2, 1)
        return x.view(inp.shape)

    def load_state_dict(self, state_dict, strict=True):
        own_state = self.state_dict()
        for name, param in state_dict.items():
            if name in own_state:
                if isinstance(param, nn.Parameter):
                    param = param.data
                try:
                    own_state[name].copy_(param)
                except Exception:
                    if name.find('tail') == -1:
                        raise RuntimeError('While copying the parameter named {}, '
                                           'whose dimensions in the model are {} and '
                                           'whose dimensions in the checkpoint are {}.'
                                           .format(name, own_state[name].size(), param.size()))
            elif strict:
                if name.find('tail') == -1:
                    raise KeyError('unexpected key "{}" in state_dict'
                                   .format(name))