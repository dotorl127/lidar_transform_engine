# modified from: https://github.com/thstkdgus35/EDSR-PyTorch

from argparse import Namespace
import torch.nn as nn
from . import models
from .models import register


class Upsampler(nn.Sequential):
    def __init__(self, scale, n_feats):
        m = [nn.ConvTranspose2d(n_feats, n_feats, 3, padding=1, stride=(scale, 1), bias=True)]

        super(Upsampler, self).__init__(*m)


class ResBlock(nn.Module):
    def __init__(self, n_feats, kernel_size, bias=True, bn=False, act=nn.ReLU(True), res_scale=1):

        super(ResBlock, self).__init__()
        m = []
        for i in range(2):
            m.append(nn.Conv2d(n_feats, n_feats, kernel_size, padding=(kernel_size//2), bias=bias))
            if bn:
                m.append(nn.BatchNorm2d(n_feats))
            if i == 0:
                m.append(act)

        self.body = nn.Sequential(*m)
        self.res_scale = res_scale

    def forward(self, x):
        res = self.body(x).mul(self.res_scale)
        res += x
        return res


class EDSR(nn.Module):
    def __init__(self, args):
        super(EDSR, self).__init__()
        self.args = args
        n_resblocks = args.n_resblocks
        n_feats = args.n_feats
        kernel_size = 3
        self.channel = 32
        act = nn.ReLU(True)

        m_head = [nn.Conv2d(args.n_colors, n_feats, kernel_size, padding=(kernel_size//2))]

        m_body = [
            ResBlock(
                n_feats, kernel_size, act=act, res_scale=args.res_scale)
            for _ in range(n_resblocks)
        ]
        m_body.append(nn.Conv2d(n_feats, n_feats, kernel_size, padding=(kernel_size//2)))

        m_tail = [nn.Conv2d(n_feats, args.n_colors, kernel_size, padding=(kernel_size//2))]

        self.head = nn.Sequential(*m_head)
        self.body = nn.Sequential(*m_body)
        self.tail = nn.Sequential(*m_tail)

    def forward(self, x):
        x = self.head(x)
        res = self.body(x)
        res += x
        for _ in range(4):
            x = self.tail(res)
        return x

    def load_state_dict(self, state_dict, strict=True):
        own_state = self.state_dict()
        for name, param in state_dict.items():
            if name in own_state:
                if isinstance(param, nn.Parameter):
                    param = param.data
                try:
                    own_state[name].copy_(param)
                except Exception:
                    if name.find('tail') == -1:
                        raise RuntimeError('While copying the parameter named {}, '
                                           'whose dimensions in the model are {} and '
                                           'whose dimensions in the checkpoint are {}.'
                                           .format(name, own_state[name].size(), param.size()))
            elif strict:
                if name.find('tail') == -1:
                    raise KeyError('unexpected key "{}" in state_dict'
                                   .format(name))

    def set_channel(self, channel):
        self.channel = channel


@register('edsr-moon')
def make_edsr_moon(n_resblocks=16, n_feats=64, res_scale=1,
                   rgb_range=1, n_colors=1):
    args = Namespace()
    args.n_resblocks = n_resblocks
    args.n_feats = n_feats
    args.res_scale = res_scale
    args.rgb_range = rgb_range
    args.n_colors = n_colors
    return EDSR(args)


class EDSR_mlp(nn.Module):
    def __init__(self, args):
        super(EDSR_mlp, self).__init__()
        self.args = args
        n_resblocks = args.n_resblocks
        n_feats = args.n_feats
        kernel_size = 3
        self.channel = 32
        act = nn.ReLU(True)

        m_head = [nn.Conv2d(args.n_colors, n_feats, kernel_size, padding=(kernel_size//2))]

        m_body = [ResBlock(n_feats, kernel_size, act=act, res_scale=args.res_scale) for _ in range(n_resblocks)]
        m_body.append(nn.Conv2d(n_feats, n_feats, kernel_size, padding=(kernel_size//2)))

        # m_tail = [nn.Conv2d(n_feats, args.n_colors, kernel_size, padding=(kernel_size//2))]

        self.head = nn.Sequential(*m_head)
        self.body = nn.Sequential(*m_body)
        # self.tail = nn.Sequential(*m_tail)
        self.imnet = models.make(args.imnet_spec, args={'in_dim': n_feats})

    def forward(self, x):
        x = self.head(x)
        res = self.body(x)
        res += x
        # x = self.tail(res)

        b, c, h, w = res.shape
        x = self.imnet(res.view(b * h * w, -1)).view(b, -1, h, w)

        return x

    def load_state_dict(self, state_dict, strict=True):
        own_state = self.state_dict()
        for name, param in state_dict.items():
            if name in own_state:
                if isinstance(param, nn.Parameter):
                    param = param.data
                try:
                    own_state[name].copy_(param)
                except Exception:
                    if name.find('tail') == -1:
                        raise RuntimeError('While copying the parameter named {}, '
                                           'whose dimensions in the model are {} and '
                                           'whose dimensions in the checkpoint are {}.'
                                           .format(name, own_state[name].size(), param.size()))
            elif strict:
                if name.find('tail') == -1:
                    raise KeyError('unexpected key "{}" in state_dict'
                                   .format(name))

    def set_channel(self, channel):
        self.channel = channel


@register('edsr-moon-mlp')
def make_edsr_moon(n_resblocks=16, n_feats=64, res_scale=1,
                   rgb_range=1, n_colors=1, imnet_spec=None):
    args = Namespace()
    args.n_resblocks = n_resblocks
    args.n_feats = n_feats
    args.res_scale = res_scale
    args.rgb_range = rgb_range
    args.n_colors = n_colors
    args.imnet_spec = imnet_spec
    return EDSR_mlp(args)
