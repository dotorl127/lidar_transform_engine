import numpy as np


def generate_laser_directions(config):
    """
    Generate the laser directions using the LiDAR specification.

    :param config: config specification
    :return: a set of the query laser directions;
    """
    v_dir = np.linspace(start=config['tgt_down_fov'], stop=config['tgt_up_fov'], num=config['tgt_ver_res'])
    h_dir = np.linspace(start=-np.pi, stop=np.pi, num=config['tgt_hoz_res'], endpoint=False)

    v_angles = []
    h_angles = []

    for i in range(config['tgt_ver_res']):
        v_angles = np.append(v_angles, np.ones(config['tgt_hoz_res']) * v_dir[i])
        h_angles = np.append(h_angles, h_dir)

    return np.stack((v_angles, h_angles), axis=-1).astype(np.float32)


def normalization_queries(queries, config):
    """
    Normalize query lasers toward input range image space.
    [src_down_fov-v_res*0.5 ~ src_up_fov+v_res*0.5] --> [-1 ~ 1]
    [min_h-h_res*0.5 ~ max_h-h_res*0.5] --> [-1 ~ 1]

    :param queries: query lasers without normalization
    :param config: input config specification
    :return: normalized query lasers
    """
    # Vertical angle: [src_down_fov-v_res*0.5 ~ src_up_fov+v_res*0.5] --> [0 ~ 1]
    v_res = (config['src_up_fov'] - config['src_down_fov']) / (config['src_ver_res'] - 1)
    src_down_fov = config['src_down_fov'] - v_res * 0.5
    src_up_fov = config['src_up_fov'] + v_res * 0.5
    queries[:, 0] -= src_down_fov
    queries[:, 0] /= (src_up_fov - src_down_fov)

    # Horizontal angle: [min_h-h_res*0.5 ~ max_h-h_res*0.5] --> [0 ~ 1]
    h_res = (np.pi * 2) / config['src_hoz_res']
    queries[:, 1] += (h_res * 0.5)
    queries[queries[:, 1] < -np.pi, 1] += (2.0 * np.pi)  # min_h == -np.pi
    queries[queries[:, 1] >= np.pi, 1] -= (2.0 * np.pi)  # max_h == +np.pi
    queries[:, 1] += np.pi
    queries[:, 1] /= (2.0 * np.pi)

    # [0 ~ 1] --> [-1 ~ 1]
    queries *= 2.0
    queries -= 1.0
    return queries


def generate_laser_directions_polar_coord(config):
    """
    Generate the laser directions using the config specification.

    :param config: config specification
    :return: a set of the query laser directions;
    """
    if config['tgt_ver_ang'] is not None:
        v_dir = np.array(config['tgt_ver_ang'])
    else:
        v_dir = np.linspace(start=np.pi / 2 - abs(config['tgt_up_fov']), stop=np.pi / 2 + abs(config['tgt_down_fov']), num=config['tgt_ver_res'])
    h_dir = np.linspace(start=np.pi, stop=-np.pi, num=config['tgt_hoz_res'], endpoint=False)

    v_angles = []
    h_angles = []

    for i in range(config['tgt_ver_res']):
        v_angles = np.append(v_angles, np.ones(config['tgt_hoz_res']) * v_dir[i])
        h_angles = np.append(h_angles, h_dir)

    return np.stack((v_angles, h_angles), axis=-1).astype(np.float32)


def range_image_to_points(range_image, config, remove_zero_range=True):
    """
    Convert a range image to the points in the sensor coordinate.

    :param range_image: denormalized range image
    :param config: config specification
    :param remove_zero_range: flag to remove the points with zero ranges
    :return: points in sensor coordinate
    """
    angles = generate_laser_directions_polar_coord(config)
    r = range_image.flatten()

    x = r * np.sin(angles[:, 0]) * np.cos(angles[:, 1])
    y = r * np.sin(angles[:, 0]) * np.sin(angles[:, 1])
    z = r * np.cos(angles[:, 0])

    points = np.stack((x, y, z), axis=-1)  # sensor coordinate

    # Remove the points having invalid detection distances
    if remove_zero_range is True:
        points = np.delete(points, np.where(r < 1e-5), axis=0)

    return points