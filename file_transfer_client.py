import grpc
import file_transfer_pb2
import file_transfer_pb2_grpc


def run():
    channel = grpc.insecure_channel("localhost:50051")
    stub = file_transfer_pb2_grpc.FileTransferStub(channel)

    request = file_transfer_pb2.TransferRequest(
        src_directory_path="/media/data_storage/datasets/moon/lidar_transform/kitti_raw_pointcloud_for_test",
        src_GT_directory_path="/media/data_storage/datasets/moon/lidar_transform/kitti_raw_pointcloud_for_test",
        tgt_directory_path="/media/data_storage/datasets/moon/lidar_transform/test_result",
        tgt_GT_directory_path="/media/data_storage/datasets/moon/lidar_transform/test_result",
        src_sensor_profile_path="/media/server_storage/repositories/moon/engine_grpc/config/lidar_info.yaml",
        tgt_sensor_profile_path="/media/server_storage/repositories/moon/engine_grpc/config/tgt_sensor_profile.yaml",
        transform_config_path="/media/server_storage/repositories/moon/engine_grpc/config/lidar_transform_config.yaml"
    )

    response = stub.Transfer(request)
    if response is not None and response.success:
        print("Transfer successful")
    else:
        print("Transfer failed")


if __name__ == "__main__":
    run()
